package ai.cuddle.ws;

import javax.websocket.ClientEndpointConfig;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Fills in the headers for the connection.
 *
 * Author: sumith.balagangadharan@cuddle.ai
 */
public class Config extends ClientEndpointConfig.Configurator {

    @Override
    public void beforeRequest(Map<String, List<String>> headers) {
        // Adds the subscription key
//        List<String> values = new ArrayList<>();
//        values.add("<Subscription Id>");

//        headers.put("Ocp-Apim-Subscription-Key", values);

        // Adds the token
        List<String> tokenValues = new ArrayList<>();
        tokenValues.add("<Token>");

        headers.put("Authorization", tokenValues);

        UUID uuid = UUID.randomUUID();
        String uuidStr = uuid.toString();
        System.out.println("UUID: " + uuidStr);

        // Adds a unique Id for the connection as required by Custom Speech Service
        List<String> connectionHeader = new ArrayList<>();
        connectionHeader.add(uuidStr);

        headers.put("X-ConnectionId", connectionHeader);

        // Adds the content type
        List<String> content = new ArrayList<>();
        content.add("audio/wav");
        content.add("codec=audio/pcm");
        content.add("samplerate=16000");

        headers.put("Content-type", content);
    }
}
