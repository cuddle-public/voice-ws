package ai.cuddle.ws;

import javax.websocket.*;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Connection configuration required for handshake.
 *
 * Author: sumith.balagangadharan@cuddle.ai
 */
@ClientEndpoint(
        configurator = Config.class
)
public class SpeechEndpoint {

    @OnOpen
    public void onOpen(Session session) {
        System.out.println("Connected to endpoint: " + session.getBasicRemote());
        try {
            RandomAccessFile file = new RandomAccessFile("<Path to the sound file>", "r");

            FileChannel inChannel = file.getChannel();

            ByteBuffer buf = ByteBuffer.allocate((int) file.length());
            int bytesRead = inChannel.read(buf);
            System.out.println("Read bytes: " + bytesRead);

            System.out.println("Sending message to endpoint");
            session.getBasicRemote().sendBinary(buf);
        } catch (IOException ex) {
            Logger.getLogger(SpeechEndpoint.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            System.out.println("Done sending");
        }
    }

    @OnMessage
    public void processMessage(String message) {
        System.out.println("Received message in client: " + message);
        App.messageLatch.countDown();
    }

    @OnClose
    public void disconnected(Session session, CloseReason reason) {
        System.out.println("Connection closed: " + reason);
    }

    @OnError
    public void processError(Throwable t) {
        t.printStackTrace();
    }
}
