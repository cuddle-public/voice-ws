This is a sample project to figure out how to communicate with Azure Custom Speech Service using WebSockets.

You need to supply the following:
1. WS end point in App.java
2. Subscription Id or token in Config.java
3. Audio File to transcribe in SpeechEndpoint.java

Current implementation connects and uploads audio to Custom Speech Service endpoint without errors, but does not get a transcription (or any communication) back from the server.

Here's a sample output you will see on the console when you run this program:
`
Connecting to <Endpoint>
UUID: aec35cb8-f3e7-4984-a07d-0107a6d8f831
Connected to endpoint: Wrapped: Basic
Read bytes: 498896
Sending message to endpoint
Done sending
`